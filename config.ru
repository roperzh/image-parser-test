require "rack"
require "rack/cors"
require "rack/parser"
require "base64"
require "json"

test_app = lambda do |env|
  req = Rack::Request.new(env)

  puts "=============== Entry ==============="

  puts "<<<<<<<<< #{req.POST['image'][0...30]}"

  File.open('img.jpg', 'wb') do|f|
    f.write(Base64.decode64(req.POST['image']))
  end

  return [200, {"Content-Type" => "text/html"}, ["out"]]
end

use Rack::Cors do
  allow do
    origins '*'
    resource '/*', :headers => :any, :methods => :post
  end
end

use Rack::Parser, :parsers => { 'application/json' => proc { |data| JSON.parse data } }

run test_app
